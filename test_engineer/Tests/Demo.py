from selenium import webdriver
import time
import datetime

def test_complete_todo(driver, key_string):
    #TODO: find element by key_string
    #      complete the element by clicking
    #      check if the element is actually completed
    try:
        item = find_matching_class_with_text('undone', key_string)
        if not item:
            return False
        # now you found the undone element
        item.find_element_by_xpath('..').find_element_by_class_name("icon").click()

        # TODO: check if the element is actually "completed"
        return True
    except:
        return False


def test_uncomplete_todo(driver, key_string):
    # TODO: find element by key_string
    #      complete the element by clicking
    #      check if the element is actually completed
    try:
        item = find_matching_class_with_text('done', key_string)
        if not item:
            return False
        # now you found the undone element
        item.find_element_by_xpath('..').find_element_by_class_name("icon").click()

        # TODO: check if the element is actually "uncompleted"

        return True
    except:
        return False

def test_destroy_todo(driver, key_string):
    # TODO: find element by key_string
    #      complete the element by clicking
    #      check if the element is actually completed
    try:
        item = find_matching_class_with_text('undone', key_string)
        print('found in undone', item)
        if not item:
            item = find_matching_class_with_text('done', key_string)
            print('found in done', item)
        if not item:
            print('did not find in undone or done')
            return False
        # now you found the undone element
        item.find_element_by_xpath('..').find_element_by_class_name("close").click()
        # TODO: check if the element is actually "uncompleted"

        return True
    except:
        return False

def find_matching_class_with_text(class_name, key_string):
    items = driver.find_elements_by_class_name(class_name)
    for item in items:
        inner_text = item.get_attribute('innerText')
        if inner_text == key_string:
            return item
    return None


def test_create_todo(driver, key_string):
    '''
    :param driver: web driver
    :param key_string: string to be tested
    :return: True if pass, False if exception

    TODO: check key_string actually exists after click on the submit.
    '''

    try:
        driver.find_element_by_class_name('form-control').send_keys(key_string)
        driver.find_element_by_class_name('submit').click()
        return True
    except Exception as e:
        return False

def print_summary(success, failures):
    print('''
    Summary: {} succss
             {} failed
    '''.format(success, failures))

def print_string(result, function, test_string, success,failures):
    function_name = function.__name__
    if result:
        indicator = 'succeeded'
        success += 1
    else:
        indicator = 'failed'
        failures += 1
    print('{} {}, test string {} --------- {}'.format(str(datetime.datetime.now()), function_name, test_string, indicator))
    return success, failures


if __name__ == '__main__':

    #create web
    driver=webdriver.Chrome()

    #request page
    driver.get(' http://localhost:3000')

    success = 0
    failures = 0

    #page click&save
    test_string1 = 'Go shopping2'
    result = test_create_todo(driver, test_string1)
    success, failures = print_string(result, test_create_todo, test_string1, success, failures)
    time.sleep(1)
    test_string2 = 'buy flowers2'
    result = test_create_todo(driver, test_string2)
    success, failures = print_string(result, test_create_todo,  test_string2, success, failures)
    time.sleep(1)
    test_string3 = 'learn react2'
    result = test_create_todo(driver, test_string3)
    success, failures = print_string(result, test_create_todo, test_string3, success, failures)
    time.sleep(2)

    # test test_complete_todo
    result = test_complete_todo(driver, test_string1)
    success, failures = print_string(result, test_complete_todo, test_string1, success, failures)
    time.sleep(1)

    result = test_uncomplete_todo(driver, test_string1)
    success, failures = print_string(result, test_uncomplete_todo, test_string1, success, failures)
    time.sleep(1)
    #
    # #mark_done
    # driver.find_element_by_class_name('icon').click()
    result = test_destroy_todo(driver, test_string1)
    success, failures = print_string(result, test_destroy_todo, test_string1, success, failures)
    time.sleep(1)


    # #obtain new date
    # print(driver.page_source)
    # print(driver.get_cookie())
    # print(driver.current_url)


    #driver.close()

    print_summary(success, failures)

